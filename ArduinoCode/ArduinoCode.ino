// Basic Arduino code to read data from two DHT
// sensors and display the results on a 16x2
// LCD screen continuously
//
// Q Reynolds 2017

// Include the necessary code for the LCD screen
// and DHT sensors
#include <LiquidCrystal.h>
#include "DHT.h"

// Spec the type of DHT sensor in use
#define DHTTYPE DHT11

// Choose the data pins that the DHTs are connected
// to
DHT dht1(8, DHTTYPE);
DHT dht2(9, DHTTYPE);

// Choose the data pins that the LCD is connected to
LiquidCrystal lcd(12, 10, 5, 4, 3, 2);

// Start up the DHT sensors and LCD display
void setup() {
  dht1.begin();
  dht1.begin();

  lcd.begin(16, 2);
  lcd.print("hello, world!");
  lcd.clear();
}

void loop() {
  // Attempt to read the humidity and temperature from
  // the two DHT sensors
  float h1 = dht1.readHumidity();
  float t1 = dht1.readTemperature();
  float h2 = dht2.readHumidity();
  float t2 = dht2.readTemperature();

  // Set the cursor position to the start of the
  // first line on the LCD screen
  lcd.setCursor(0,0);
  // Check if garbage was read from DHT #1, if so
  // then print an error message to the LCD, 
  // otherwise print the results
  if (isnan(h1) || isnan(t1))
  {
    lcd.print("Sn1 read failed    ");
  }
  else 
  {
    lcd.print("Sn1: ");
    lcd.print(h1, 0);
    lcd.print("% ");
    lcd.print(t1, 0);
    lcd.print("*C    ");
  }

  // Set the cursor position to the start of the
  // second line on the LCD
  lcd.setCursor(0,1);
  // Check if garbage was read from DHT #2, if so
  // then print an error message to the LCD, 
  // otherwise print the results
  if (isnan(h2) || isnan(t2))
  {
    lcd.print("Sn2 read failed    ");
  }
  else 
  {
    lcd.print("Sn2: ");
    lcd.print(h2,0);
    lcd.print("% ");
    lcd.print(t2,0);
    lcd.print("*C    ");
  }
  
  // Wait 2s for the DHTs to update their
  // readings
  delay(2000);
}

